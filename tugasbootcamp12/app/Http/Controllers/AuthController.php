<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showRegisterForm()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        $data = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'gender' => 'required|string',
            'nationality' => 'required|string',
            'language' => 'required|array',
            'bio' => 'nullable|string',
        ]);

        return redirect()->route('welcome')->with('data', $data);
    }

    public function welcome(Request $request)
    {
        $data = $request->session()->get('data');

        return view('welcome', compact('data'));
    }
}
