<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="{{ route('register') }}" method="POST">
        @csrf
        <label for="first_name">First Name:</label><br>

        <input type="text" name="first_name" required><br><br>

        <label for="last_name">Last Name:</label><br>
        <input type="text" name="last_name" required><br><br>

        <label for="gender">Gender:</label>
        <input type="radio" name="gender" value="Man" required> Man
        <input type="radio" name="gender" value="Woman" required> Woman
        <input type="radio" name="gender" value="Other" required> Other<br><br>

        <label for="nationality">Nationality:</label>
        <select name="nationality" required>
            <option value="Indonesia">Indonesia</option>
            <option value="Other">Other</option>
        </select><br><br>

        <label for="language">Language Spoken:</label><br>
        <input type="checkbox" name="language[]" value="Bahasa Indonesia"> Bahasa Indonesia<br>
        <input type="checkbox" name="language[]" value="English"> English<br>
        <input type="checkbox" name="language[]" value="Arabic"> Arabic<br>
        <input type="checkbox" name="language[]" value="Japanese"> Japanese<br><br>

        <label for="bio">Bio:</label><br>
        <textarea name="bio"></textarea><br><br>

        <button type="submit">Sign Up</button>
    </form>
</body>
</html>
